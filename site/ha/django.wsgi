import os
import sys

sys.path.append('/home/pi/django_site/automagically')

os.environ['MPLCONFIGDIR'] = '/tmp/'
os.environ['DJANGO_SETTINGS_MODULE'] = 'ha.settings_apache'

import django.core.handlers.wsgi

application = django.core.handlers.wsgi.WSGIHandler()
