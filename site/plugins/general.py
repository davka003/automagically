from django.db import models
from settings.models import getSettings
import time
import os

PLUGIN_NAME = os.path.splitext(os.path.split(__file__)[1])[0] #This will be the filename without .py

global generalSettings
generalSettings = {}

def signalHandler(signal):
    global generalSettings
    if not generalSettings or signal.content.strip() == PLUGIN_NAME + ',configuration,changed':
        generalSettings = getSettings(PLUGIN_NAME)
        tzConfigFile = os.path.join(os.path.split(os.path.split(__file__)[0])[0], 'ha', 'timezone')
        current = open(tzConfigFile, 'r').read().strip()
        if current != generalSettings['Timezone']:
            open(tzConfigFile, 'w').write(generalSettings['Timezone'])
            print 'Timezone updated'


def getSetting(setting):
    global generalSettings
    if not generalSettings:
        generalSettings = getSettings(PLUGIN_NAME)

    return generalSettings.get(setting, None)

#Note that this function will be called from its own content inside webserver and will not have access to other things in this file
def validateTimezone(setting, value):
    import os
    if os.path.exists(os.path.join('/usr/share/zoneinfo', value)):
        return ''
    else:
        print 'Path does not exist'
        return 'Invalid timezone please consult http://en.wikipedia.org/wiki/List_of_tz_zones_by_name for valid ones'

            
def init():
    settings = {'Location Latitude':   ('float', 65.57777),
                'Location Longitude': ('float', 22.2574),
                'Timezone': ('string', 'Europe/Stockholm', validateTimezone)}

    return ('general', settings, signalHandler, None)

