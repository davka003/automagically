from django.db import models
from core.models import Event, ScheduledEvent
import signals.models
import time
import datetime
import threading
from pytz import timezone
from django.conf import settings

PLUGIN_NAME = 'timedevent'

debug = False

ev = threading.Event()
keepRunning = True

def signalHandler(signal):
    if signal.content.startswith('timedevent,changed'):
        print 'timed event changed received'
        ev.set()

    elif signal.content == 'terminate':
        global keepRunning
        keepRunning = False
        ev.set()

def doTimedEvents():
    if debug:
        print time.time(), 'do timed events'
    local = timezone(settings.TIME_ZONE)

    #Find events has happend
    events = Event.objects.filter(doAt__lte = local.localize(datetime.datetime.now()))
    scheduledEventsToUpdate = []

    for e in events:
        if debug:
            print 'Event to do', e

        if e.schedule:
            scheduledEventsToUpdate.append(e.schedule)

        realyDo = False

        if e.condition:
            if e.condition.dataType == 1: #String
                if e.condition.getValue() != '':
                    realyDo = True
            elif e.condition.dataType == 1: #INT
                value = int(e.condition.getValue())
                if value != 0 and value != -9999:
                    realyDo = True
            elif e.condition.dataType == 2: #FLOAT
                value = float(e.condition.getValue())
                if value != 0.0 and value != -9999.99:
                    realyDo = True
            elif e.condition.dataType == 3: #BOOLEAN
                value = e.condition.getValue()
                if value == 'True':
                    realyDo = True
        else:
            if debug:
                print 'No condition needs to be applied'
            realyDo = True

        if debug:
            print 'Condition applied, result realyDo =', realyDo

        if realyDo:
            e.device.execute(e.command)

        e.delete()

    for s in scheduledEventsToUpdate:
        if debug:
            print 'Need to create event for', s
        s.scheduleNext()


    try:
        event = Event.objects.order_by('doAt')[0]
        deltaTime = event.doAt - local.localize(datetime.datetime.now())
        if debug:
            print 'delta', repr(deltaTime)
            print deltaTime.seconds
            print deltaTime.days
        if deltaTime.days > 0:
            newWaitTime = 24*60*60 #never wait more than a day
        elif deltaTime.days < 0:
            newWaitTime = 10
        else:
            newWaitTime = deltaTime.seconds
    except:                
        newWaitTime = 60

    return newWaitTime

def threadFunc():
    global keepRunning
    newWaitTime = 10
    while(keepRunning):
        try:
            if debug:
                print 'Time to wait on condition', newWaitTime
            ev.wait(newWaitTime)
            ev.clear()
            newWaitTime = doTimedEvents()            

        except:
            print 'Error in timedevents'
            raise


def init():
    settings = {}

    return (PLUGIN_NAME, settings, signalHandler, threadFunc)

