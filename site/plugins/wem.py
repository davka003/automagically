#
#
# File:         wem.py
# Version:      0.9
#
# Description:  An Automagically plugin for the Wireless Energy Meter, 
#               from http://foogadgets.tictail.com
#
# Usage:        Install in ~/source/automagically/site/plugins/, restart Automagically
#
# Listens to:   tellstick,raw:class:sensor;protocol:fineoffset
#
# Provides:     wem,data;status:OK;datetime:TIME;seq:XX;energy:YY;packetloss:ZZ;pmom:WW;
#               When having data to send
#               - datetime = Time the signal was received in Automagically, e.g
#                            [2014-02-26T21:33:52.274328 or 1393829507.623416] depending on setting
#                            where one is human readable and the other is seconds since the epoch
#               - seq = The received sequence number, e.g. "56"
#               - energy = The energy measured since last report, reported in Wh, e.g. "23.2" Wh
#               - packetloss = Indication if there was packet-loss, e.g. "0"
#                   - 0 = No packet loss
#                   - 1 = One packet lost on primary channel, managed to recover
#               - Pmom = Instantaneous power, reported in kW. e.g. "0,99561" kW
#               wem,data;status:TO_MANY_LOST_PACKAGES;
#               When there are too many lost packages, might be good to look at your set-up
#
# Signals:      wem,action:timeout - In manual mode, simulate a timeout, only for debugging
#               wem,action:firstdayofmonth - responds with wem,info;firstdayofmonth:true or wem,info;firstdayofmonth:false
#               wem,action:firstdayofyear - responds with wem,info;firstdayofyear:true or wem,info;firstdayofyear:false
#
# Pattern to 
# match:        wem,data;status:%s;datetime:%s;seq:%d;energy:%f;packetloss:%d;pmom:%f;
#               $1 - status
#               $2 - datetime
#               $3 - seq [no.]
#               $4 - energy [Wh]
#               $5 - packetloss [no.]
#               $6 - pmom [kW]
#
#

# Imports
from django.db import models
from settings.models import getSettings
import signals.models
import Queue
import threading
import time
import datetime
import os

# Plugin configurations
PLUGIN_NAME = 'wem'
PLUGIN_ENABLED =  'Enabled'
PLUGIN_PRIMARY_ID =  'primaryId'
PLUGIN_SECONDARY_ID =  'secondaryId'
PLUGIN_KW_PER_BLINK =  'blinksPerWatt'
PLUGIN_MANUAL = 'ManualMode'
PLUGIN_TIMEFORMAT = 'humanReadableTime'
PLUGIN_LOGTOFILE = 'LogToFile'
PLUGIN_LOGTOFILE_SEP = 'LogToFileSeparator'
PLUGIN_LOGTOFILE_COMMA = 'LogToFileComma'

# Plugin settings
PLUGIN_SIGNAL_LISTEN = "tellstick,raw:class:sensor;protocol:fineoffset;"
PLUGIN_MAX_TIME_BETWEEN_SIGNALS = 1 # MUST NOT BE > 60, BETTER STAY BELOW 10
PLUGIN_SIGNAL_SEND = "wem,data;status:"

# Global variables
debug = False
workQueue = Queue.Queue()
fileName = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../logs/wem.csv'))


def logToWemFile(logFileSeparator, logFileComma, status, input=None, packetloss=None, time_delta=60, humanReadableTime=True): # str, [val, seq, time, valid], int, int, boolean
    if debug:
        print "logToWemFile" + status
    if status == "OK":
        pmom = round(input[0] * 1/1000.0 * 60 * 60/float(time_delta), 5)
        if humanReadableTime:
            dateReport = str(datetime.datetime.fromtimestamp(input[2]).isoformat())
        else:
            dr = '%6f' % input[2]
            dateReport = dr.replace('.',logFileComma)
        # status, date, sequence, energy, pmom, packeloss, pmom
        csvLine = status + logFileSeparator + \
                  dateReport + logFileSeparator + \
                  str(input[1]) + logFileSeparator + \
                  str(input[0]).replace('.',logFileComma) + logFileSeparator + \
                  str(pmom).replace('.',logFileComma) + logFileSeparator + \
                  str(packetloss) + "\n"
    else:
        csvLine = status + "\n"

    try:
        if debug:
            print "Writing: " + csvLine
            print "To: " + fileName
        file = open(fileName, "a")
        file.write(csvLine)
        file.close()
    except:
        print "Could not write to the file"

def buildOutSignal(status, input=None, packetloss=None, time_delta=60, humanReadableTime=True): # str, [val, seq, time, valid], int, int, boolean
    if status == "OK":
        # Calculate 
        if debug:
            print "Pmom calculates E=" + str(input[0]) + " and time_delta=" + str(time_delta)
        pmom = round(input[0] * 1/1000.0 * 60 * 60/float(time_delta), 5)
        
        if humanReadableTime:
            dateReport = str(datetime.datetime.fromtimestamp(input[2]).isoformat())
        else:
            dateReport = '%6f' % input[2]
        ret = PLUGIN_SIGNAL_SEND + status + ";datetime:" + dateReport + \
                                            ";seq:" + str(input[1]) + \
                                            ";energy:" + str(input[0]) + \
                                            ";packetloss:" + str(packetloss) + \
                                            ";pmom:" + str(pmom) + \
                                            ";"
    else:
        ret = PLUGIN_SIGNAL_SEND + status
    
    return ret
    

class EnergyTh ( threading.Thread ):
    timeDelta = 60
    humanReadable = True
    logToFile = False
    logFileSeparator = ";"
    logFileComma = ","

    primaryInputData     = [0.0, 0, 0, False] #[val, seq, time, valid]
    secondaryInputData   = [0.0, 0, 0, False] #[val, seq, time, valid]

    primarySavedData     = [0.0, 0, 0, False] #[val, seq, time, valid]
    secondarySavedData   = [0.0, 0, 0, False] #[val, seq, time, valid]

    def setPrimaryData(self, val, seq, timestamp):
        if debug:
            print "Setting primary input data to " + str([val, seq, timestamp, True])
        EnergyTh.primaryInputData = list([val, seq, timestamp, True])

    def setSecondaryData(self, val, seq, timestamp):
        if debug:
            print "Setting secondary input data to " + str([val, seq, timestamp, True])
        EnergyTh.secondaryInputData = list([val, seq, timestamp, True])

    def setSettings(self, humanReadableTimeformat, logMeteringToFile, logMeteringFileSeparator, logMeteringFileComma):
        EnergyTh.humanReadable = humanReadableTimeformat
        EnergyTh.logToFile = logMeteringToFile
        EnergyTh.logFileSeparator = logMeteringFileSeparator
        EnergyTh.logFileComma = logMeteringFileComma
    
    def run (self):         
        # This function is called approximately every 60 sec, independent if signals has been received.
        # Wait for two signals to arrive before acting
        time.sleep(PLUGIN_MAX_TIME_BETWEEN_SIGNALS) 
        out_sig = ''

        if debug:
            print "EnergyTh PrimData: " + str(EnergyTh.primaryInputData) + ", SecData: " + str(EnergyTh.secondaryInputData)
            print "EnergyTh PrimSave: " + str(EnergyTh.primarySavedData) + ", SecSave: " + str(EnergyTh.secondarySavedData)
       
        #
        # The intelligence
        #
        if EnergyTh.primarySavedData[3]:       
            if EnergyTh.secondaryInputData[3]:
                packetLoss = 0
            else:
                packetLoss = 1
                       
            out_sig = buildOutSignal("OK", EnergyTh.primarySavedData, packetLoss, EnergyTh.timeDelta, EnergyTh.humanReadable)
            if EnergyTh.logToFile:
                logToWemFile(EnergyTh.logFileSeparator, EnergyTh.logFileComma, "OK", EnergyTh.primarySavedData, packetLoss, EnergyTh.timeDelta, EnergyTh.humanReadable)
        else:
            # A primary packet was lost
            packetLoss = 1
            
            # Can we recover
            if EnergyTh.secondaryInputData[3]:
                # Yes, we could recover
                
                # Adjust timestamp
                tempSec = list(EnergyTh.secondaryInputData)
                tempSec[2] = tempSec[2] - EnergyTh.timeDelta
                out_sig = buildOutSignal("OK", tempSec, packetLoss, EnergyTh.timeDelta, EnergyTh.humanReadable)
                if EnergyTh.logToFile:
                    logToWemFile(EnergyTh.logFileSeparator, EnergyTh.logFileComma,"OK", tempSec, packetLoss, EnergyTh.timeDelta, EnergyTh.humanReadable)
            else:
                # No, two consecutive packages lost
                out_sig = buildOutSignal("TO_MANY_LOST_PACKAGES")
                if EnergyTh.logToFile:
                    logToWemFile(EnergyTh.logFileSeparator, EnergyTh.logFileComma, "TO_MANY_LOST_PACKAGES")
        
        if len(out_sig) > 0:
            #Send the signal
            signals.models.postToQueue(out_sig, PLUGIN_NAME)

        # Save time_diff until next iteratation
        if EnergyTh.primarySavedData[3]:
            if EnergyTh.primaryInputData[3]:
                EnergyTh.timeDelta = EnergyTh.primaryInputData[2] - EnergyTh.primarySavedData[2]
            elif EnergyTh.secondaryInputData[3]:
                EnergyTh.timeDelta = EnergyTh.secondaryInputData[2] - EnergyTh.primarySavedData[2]
        elif EnergyTh.secondarySavedData[3]:
            if EnergyTh.primaryInputData[3]:
                EnergyTh.timeDelta = EnergyTh.primaryInputData[2] - EnergyTh.secondarySavedData[2]
            elif EnergyTh.secondaryInputData[3]:
                EnergyTh.timeDelta = EnergyTh.secondaryInputData[2] - EnergyTh.secondarySavedData[2]

        # Buffer data for next iteration
        EnergyTh.primarySavedData = list(EnergyTh.primaryInputData)
        EnergyTh.secondarySavedData = list(EnergyTh.secondaryInputData)
       
        
        # Reset input for next iteration
        EnergyTh.primaryInputData = [0, 0, 0, False]
        EnergyTh.secondaryInputData = [0, 0, 0, False]
        
        
# Signal handler
def signalHandler(signal):
    if debug:
        print PLUGIN_NAME + ' received signal:' + signal.content.strip()
    if signal.content == 'terminate':
        workQueue.put(None)
    elif signal.content.strip() == PLUGIN_NAME + ',configuration,changed':
        workQueue.put('update')
    elif signal.content.startswith(PLUGIN_NAME + ',action:'): # Note: Change to your needs
        # Received an 'action' signal
        for i in signal.content.strip()[11:].split(';'):      # Note: 16 = len("wem,action:"), change to match your plugin name and needs
            if i != '':
                workQueue.put(i)
    elif signal.content.startswith(PLUGIN_SIGNAL_LISTEN):     # "tellstick,raw:class:sensor;protocol:fineoffset;id:69;model:temperaturehumidity;humidity:5;temp:45.1;"
        # Received a 'tellstick,raw' signal
        s = signal.content.strip()[47:].split(';')      # Note: 47 = len("tellstick,raw:class:sensor;protocol:fineoffset;")
        if len(s) == 5 or len(s) == 4: # With our without trailing ';'
            out_sig =[]
            for keyVal in s:
                if keyVal != '':
                    [key, val] = keyVal.split(':')
                    if key == 'id' or key == 'temp' or key == 'humidity':
                        out_sig.append(val)
            out_sig.append(signal.timestamp)
            if len(out_sig) == 4:
                workQueue.put(out_sig) # ['id', 'value', 'counter', timestamp]
 
# Thread function
def threadFunc():
    currentSettings = {}
    keepRunning = True
    interleaveThread = EnergyTh()

    # Load plugin settings
    currentSettings = getSettings(PLUGIN_NAME)
    pluginEnabled = currentSettings[PLUGIN_ENABLED]
    manualMode = currentSettings[PLUGIN_MANUAL]
    primaryId = currentSettings[PLUGIN_PRIMARY_ID]
    secondaryId = currentSettings[PLUGIN_SECONDARY_ID]
    kwMultiplier = 10000/currentSettings[PLUGIN_KW_PER_BLINK] 
    interleaveThread.setSettings(   currentSettings[PLUGIN_TIMEFORMAT], 
                                    currentSettings[PLUGIN_LOGTOFILE],
                                    currentSettings[PLUGIN_LOGTOFILE_SEP],
                                    currentSettings[PLUGIN_LOGTOFILE_COMMA])
    
    # Main loop
    while(keepRunning):
        try:
            try:
                if manualMode:
                    s = workQueue.get(True) # Wait for a signal
                else:
                    s = workQueue.get(True, 70) # Wait for a signal
            except:
                # Exception occurs when nextUpdate time has elapsed
                s = 'TIMEOUT'

            if s == None: # Time to close down
                if debug:
                    print PLUGIN_NAME + ' time to close down'
                workQueue.task_done()
                keepRunning = False
                continue
                
            elif s == 'update': # Configuration has changed, update to new values
                if debug:
                    print PLUGIN_NAME + ' settings updated'
                currentSettings = getSettings(PLUGIN_NAME)
                manualMode = currentSettings[PLUGIN_MANUAL]
                pluginEnabled = currentSettings[PLUGIN_ENABLED]
                primaryId = currentSettings[PLUGIN_PRIMARY_ID]
                secondaryId = currentSettings[PLUGIN_SECONDARY_ID]               
                kwMultiplier = 10000/currentSettings[PLUGIN_KW_PER_BLINK]
                interleaveThread.setSettings(   currentSettings[PLUGIN_TIMEFORMAT], 
                                                currentSettings[PLUGIN_LOGTOFILE],
                                                currentSettings[PLUGIN_LOGTOFILE_SEP],
                                                currentSettings[PLUGIN_LOGTOFILE_COMMA])
                workQueue.task_done()
                continue
                
            elif s == 'TIMEOUT': # No signals received within time-out
                # No signals received the last 60+ seconds, kick-in manual handling
                if debug:
                    print "Timeout in WEM"
                if not interleaveThread.isAlive():
                    interleaveThread = EnergyTh()
                    interleaveThread.start()
                # Note: This part should NOT have a workQueue.task_done()
                continue
            
            else: # A signal was received; handles list ['id', 'value', 'counter', timestamp] and str
                if debug:
                    print PLUGIN_NAME + ' got signal:' + str(s)
                if pluginEnabled:
                    if len(s) == 4 and type(s) == list:
                        if float(s[2]) < 0.1:
                            newVal = 204.8 - float(s[2])
                        else:
                            newVal = float(s[2])
                            
                        if primaryId == int(s[0]):
                            interleaveThread.setPrimaryData(float(newVal * kwMultiplier), int(s[1]), s[3])
                        elif secondaryId == int(s[0]):
                            interleaveThread.setSecondaryData(float(newVal * kwMultiplier), int(s[1]), s[3])
                            
                        # We have at least one signal, start the execution thread
                        if not interleaveThread.isAlive():
                            interleaveThread = EnergyTh()
                            interleaveThread.start()
                    elif type(s) == str:
                        _s=s.split(',')
                        if len(_s) == 1:
                            # Manual timeout
                            if _s[0] == 'timeout':
                                if manualMode:
                                    workQueue.put('TIMEOUT')
                            elif _s[0] == 'firstdayofmonth':
                                if debug:
                                    print "Is it 1st of this month?"
                                out_str = "wem,info;firstdayofmonth:" + ("false","true")[time.localtime().tm_mday == 1]   
                                signals.models.postToQueue(out_str, PLUGIN_NAME)

                            elif _s[0] == 'firstdayofyear':
                                if debug:
                                    print "Is it 1st of Jan?"
                                out_str = "wem,info;firstdayofyear:" + ("false","true")[time.localtime().tm_mday == 1 and time.localtime().tm_mon == 1]
                                signals.models.postToQueue(out_str, PLUGIN_NAME)
                            
                                
                    
                workQueue.task_done()
                continue

        except:
            print 'Error in ' + PLUGIN_NAME + ' threadfunction'
            if debug:
                raise

def init():
    settings = {PLUGIN_ENABLED:         ('boolean', False),
                PLUGIN_MANUAL:          ('boolean', False),
                PLUGIN_PRIMARY_ID:      ('integer', 1),
                PLUGIN_SECONDARY_ID:    ('integer', 2),
                PLUGIN_KW_PER_BLINK:    ('integer', 1000), # Major brands sends 1000 or 10000 blinks per kW
                PLUGIN_TIMEFORMAT:      ('boolean', True),
                PLUGIN_LOGTOFILE:       ('boolean',False),
                PLUGIN_LOGTOFILE_SEP:   ('string', ';'),
                PLUGIN_LOGTOFILE_COMMA: ('string', ",")
                }
    return ({PLUGIN_NAME, PLUGIN_SIGNAL_LISTEN}, settings, signalHandler, threadFunc)
