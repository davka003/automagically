# Imports
from django.db import models
from settings.models import getSettings
import signals.models
import Queue

# Plugin configurations
PLUGIN_NAME = 'template'
PLUGIN_ENABLED =  'Enabled'

# Global variables
debug = False
workQueue = Queue.Queue()

# Signal handler
def signalHandler(signal):
    if debug:
        print PLUGIN_NAME + ' received signal:' + signal.content.strip()
    if signal.content == 'terminate':
        workQueue.put(None)
    elif signal.content.strip() == PLUGIN_NAME + ',configuration,changed':
        workQueue.put('update')
    elif signal.content.startswith(PLUGIN_NAME + ',action:'): # Note: Change to your needs
        # Received an 'action' signal
        for i in signal.content.strip()[16:].split(';'):      # Note: 16 = len("template,action:"), change to match your plugin name and needs
            if i != '':
                workQueue.put(i)

# Thread function
def threadFunc():
    currentSettings = {}
    keepRunning = True
    nextUpdate = 60 #seconds

    # Load plugin settings
    currentSettings = getSettings(PLUGIN_NAME)
    pluginEnabled = currentSettings[PLUGIN_ENABLED]
    
    # Main loop
    while(keepRunning):
        try:
            try:
                if pluginEnabled and nextUpdate > 0:
                    s = workQueue.get(True, nextUpdate) # Wait for a signal or a timeout
                else:
                    s = workQueue.get(True) # Wait for a signal
                
            except:
                # Exception occurs when nextUpdate time has elapsed
                s = 'TIMEOUT'
                if debug:
                    print PLUGIN_NAME + ' time to act'

            if s == None: # Time to close down
                if debug:
                    print PLUGIN_NAME + ' time to close down'
                workQueue.task_done()
                keepRunning = False
                continue
                
            elif s == 'update': # Configuration has changed, update to new values
                if debug:
                    print PLUGIN_NAME + ' settings updated'
                currentSettings = getSettings(PLUGIN_NAME)
                pluginEnabled = currentSettings[PLUGIN_ENABLED]
                
                ##
                ## CODE HERE TO ACT WHEN CONFIGURATION CHANGES OCCURS
                ##
                
                workQueue.task_done()
                continue
    
            elif s == 'TIMEOUT':
                if debug:
                    print PLUGIN_NAME + ' acting on timeout'
                if pluginEnabled:
                    ##
                    ## CODE HERE TO ACT ON REGULAR INTERVAL
                    ##
                    print "Acting on interval for plugin " + PLUGIN_NAME
                
                # Note: This part should NOT have a workQueue.task_done()
                continue
                
            else: # A signal was received
                if debug:
                    print PLUGIN_NAME + ' got signal:' + s
                if pluginEnabled:
                    ##
                    ## CODE HERE TO ACT WHEN A SIGNAL IS RECEIVED
                    ##
                    print "Acting on signal for plugin " + PLUGIN_NAME                        
                    
                workQueue.task_done()
                continue

        except:
            print 'Error in ' + PLUGIN_NAME + ' threadfunction'
            if debug:
                raise

def init():
    settings = {PLUGIN_ENABLED:('boolean', False)}
    return (PLUGIN_NAME, settings, signalHandler, threadFunc)
