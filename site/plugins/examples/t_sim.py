#
# File:         t_sim.py
# Version:      0.6
#
# Description:  An Automagically simulator behaving like the Wireless Energy Meter 
#               by http://foogadgets.tictail.com
#
# Usage:        Install in ~/source/automagically/site/plugins/, restart automagically
#               the simulator will now send sensor events on the signal usin the same 
#               format and protocol as the real device would
#
# Signals:      t_sim,action:drop_prim,N - Number of packages that shall be simulated lost in transmission for primary channel.
#                                          Replace N with number of dropped packages to simulate.
#
#               t_sim,action:drop_sec,N  - Number of packages that shall be simulated lost in transmission for secondary channel.
#                                          Replace N with number of dropped packages to simulate.
#
#               t_sim,action:set_seq,N   - Set the sequence number of the primary channel. The secondary channel will be set to N - 1
#                                          Replace N with wanted sequence number between 0 and 99
#
#               t_sim,action:restart     - Restart the simulator by resetting counters and simulate start-up behaviour
#
#               t_sim,action:send        - When interval is 0, use this to initiate the next signal
#
#               t_sim,action:negmode     - Toggles sending negative values to test Energy Meter's MSB wrap
#
#               t_sim,action:negzero     - Sends next prim as -0.0
#

# Imports
from django.db import models
from settings.models import getSettings
import signals.models
import Queue
import random

# Plugin configurations
PLUGIN_NAME = 't_sim'
PLUGIN_ENABLED =  'Enabled'
PLUGIN_INTERVAL = 'Interval'
PLUGIN_RAND_SLEEP = 'RandomSleep'

# Plugin generic settings
PLUGIN_START_COUNTER_PRIM = 99
PLUGIN_START_COUNTER_SEC = 98
# Start-up behaviour of secondary channel, 
# 0 means send value as sequence 99. 
# 1 means not sending any package with first primary package
PLUGIN_SEC_START_DROP = 1       
PLUGIN_DUMMY_VAL = 99999

# Global variables
debug = False
workQueue = Queue.Queue()

# Signal handler
def signalHandler(signal):
    if debug:
        print PLUGIN_NAME + ' received signal:' + signal.content.strip()
    if signal.content == 'terminate':
        workQueue.put(None)
    elif signal.content.strip() == PLUGIN_NAME + ',configuration,changed':
        workQueue.put('update')
    elif signal.content.startswith(PLUGIN_NAME + ',action:'): # Note: Change to your needs
        # Received an 'action' signal
        for i in signal.content.strip()[13:].split(';'):      # Note: 16 = len("t_sim,action:"), change to match your plugin name and needs
            if i != '':
                workQueue.put(i)

# Thread function
def threadFunc():
    currentSettings = {}
    keepRunning = True
    negMode = False
    negZero = False

    primaryEvent   = "tellstick,raw:class:sensor;protocol:fineoffset;id:68;model:temperaturehumidity;humidity:"
    secondaryEvent = "tellstick,raw:class:sensor;protocol:fineoffset;id:69;model:temperaturehumidity;humidity:"

    primCounter = PLUGIN_START_COUNTER_PRIM
    secCounter = PLUGIN_START_COUNTER_SEC

    primVal = int(random.uniform(10,40) * 10) / 10.0
    secVal = int(random.uniform(10,40) * 10) / 10.0
    
    dropPrim = 0
    dropSec = 0
    
    # Load plugin settings
    currentSettings = getSettings(PLUGIN_NAME)
    pluginEnabled = currentSettings[PLUGIN_ENABLED]
    nextUpdate = currentSettings[PLUGIN_INTERVAL]
    randSleep = currentSettings[PLUGIN_RAND_SLEEP]
    
    # Main loop
    while(keepRunning):
        try:
            try:
                if pluginEnabled and nextUpdate > 0:
                    if randSleep:
                        update = nextUpdate - 1 + int(random.uniform(1,4))
                    else:
                        update = nextUpdate
                    s = workQueue.get(True, update) # Wait for a signal or a timeout
                else:
                    s = workQueue.get(True) # Wait for a signal
                
            except:
                # Exception occurs when nextUpdate time has elapsed
                s = 'TIMEOUT'

            if s == None: # Time to close down
                if debug:
                    print PLUGIN_NAME + ' time to close down'
                workQueue.task_done()
                keepRunning = False
                continue
                
            elif s == 'update': # Configuration has changed, update to new values
                if debug:
                    print PLUGIN_NAME + ' settings updated'
                currentSettings = getSettings(PLUGIN_NAME)
                pluginEnabled = currentSettings[PLUGIN_ENABLED]
                nextUpdate = currentSettings[PLUGIN_INTERVAL]
                randSleep = currentSettings[PLUGIN_RAND_SLEEP]
                workQueue.task_done()
                continue
    
            elif s == 'TIMEOUT':
                if debug:
                    print PLUGIN_NAME + ' acting on timeout'
                if pluginEnabled:
                    if debug:
                        print PLUGIN_NAME + ' time to act'

                    if dropSec == 0:
                        if secVal == PLUGIN_DUMMY_VAL:
                            signals.models.postToQueue(secondaryEvent + str(secCounter) + ";temp:" + "-0.0" + ";", PLUGIN_NAME)                            
                        else:
                            signals.models.postToQueue(secondaryEvent + str(secCounter) + ";temp:" + str(secVal) + ";", PLUGIN_NAME)
                    else:
                        # Error(s) were induced, now revert back to normal
                        dropSec = dropSec - 1

                    if dropPrim == 0:
                        if negZero:
                            negZero=False
                            primVal=PLUGIN_DUMMY_VAL
                            signals.models.postToQueue(primaryEvent + str(primCounter) + ";temp:" + "-0.0" + ";", PLUGIN_NAME)
                        else:
                            signals.models.postToQueue(primaryEvent + str(primCounter) + ";temp:" + str(primVal) + ";", PLUGIN_NAME)
                    else:
                        # Error(s) were induced, now revert back to normal
                        dropPrim = dropPrim - 1
                    
                    secCounter = primCounter
                    secVal = primVal
                    
                    primVal = int(random.uniform(10,40) * 10) / 10.0
                    if negMode:
                        primVal = 0 - primVal
                        
                    primCounter = primCounter + 1
                    if primCounter == 100:
                        primCounter = 0
                        
                # Note: This part should NOT have a workQueue.task_done()
                continue
                
            else: # A signal was received, expecting: "drop_prim,N", "drop_sec,N", "set_seq,N", "restart"
                if debug:
                    print PLUGIN_NAME + ' got signal:' + s
                if pluginEnabled:
                    _s=s.split(',')
                    if len(_s) == 2:
                        if int(_s[1]) > 0:
                            # Drop primary channel package(s)
                            if _s[0] == 'drop_prim':
                                dropPrim = int(float(_s[1]))
                            # Drop secondary channel package(s)
                            elif _s[0] == 'drop_sec':
                                dropSec = int(float(_s[1]))
                            # Set sequence number
                            elif _s[0] == 'set_seq':
                                new_num = int(float(_s[1]))
                                if new_num >= 0 and new_num <= 99:
                                    primCounter = new_num
                                    secCounter = primCounter - 1
                                    if secCounter < 0:
                                        secCounter = 99
                    elif len(_s) == 1:
                        # Restart counting
                        if _s[0] == 'restart':
                            primCounter = 99
                            secCounter = 98
                            dropSec = 0

                        if _s[0] == 'send':
                            workQueue.put('TIMEOUT')

                        if _s[0] == 'negzero':
                            negZero = True

                        if _s[0] == 'negmode':
                            if negMode:
                                negMode = False
                            else:
                                negMode = True
                  
                workQueue.task_done()
                continue

        except:
            print 'Error in ' + PLUGIN_NAME + ' threadfunction'
            if debug:
                raise

def init():
    settings = {PLUGIN_ENABLED:     ('boolean', False),
                PLUGIN_RAND_SLEEP:  ('boolean', False),
                PLUGIN_INTERVAL:    ('integer', 60)}
    return (PLUGIN_NAME, settings, signalHandler, threadFunc)
