# -*- coding: utf-8 -*-
from django.db import models
from core.models import GlobalVariable
from settings.models import getSettings
from general import getSetting
import signals.models
import Queue
import json, time
import string, re
import httplib

PLUGIN_NAME = 'xively'

# Keys for settings
XIVELY_FEED_ID =  'FeedId'
XIVELY_API_KEY =  'ApiKey'
XIVELY_INTERVAL = 'Interval'
XIVELY_ENABLED =  'Enabled'

global currentSettings
currentSettings = {}
debug = False

workQueue = Queue.Queue()

def signalHandler(signal):
    if debug:
        print "Got signal:" + signal.content
    if signal.content == 'terminate':
        workQueue.put(None)
    elif signal.content.strip() == PLUGIN_NAME + ',configuration,changed':
        workQueue.put('update')
    elif signal.content.strip() == 'general,configuration,changed':
        workQueue.put('generalupdate')
    elif signal.content.startswith(PLUGIN_NAME+',report:'):
        # Receiving xively,report:NAME:VALUE:UNIT,NAME:VALUE:UNIT,...;
        for i in signal.content.strip()[14:].split(';'):
            if i != '':
                workQueue.put(i) # Sending: NAME:VALUE:UNIT,NAME:VALUE:UNIT,...

def allowedCharactersTransform(inputStr):
    # Allowed name characters in Xively is a bit limited
    if debug:
        print "Replacing characters in " + inputStr
    inputStr = re.sub('=', '-', inputStr)
    inputStr = re.sub(' ', '_', inputStr)
    inputStr = re.sub(u'[^a-zA-Z0-9åäöÅÄÖ\n\.\-\+\_]', '', inputStr, flags=re.UNICODE)
    return inputStr
                
def threadFunc():
    keepRunning = True

    # Load settings
    lat=str(getSetting('Location Latitude'))
    lon=str(getSetting('Location Longitude'))
    
    # Load plugion settings
    currentSettings = getSettings(PLUGIN_NAME)
    xivelyEnabled = currentSettings[XIVELY_ENABLED]
    nextUpdate = currentSettings[XIVELY_INTERVAL]
    xivelyApi = "api.xively.com"
    xivelyPage = "/v2/feeds/" + currentSettings[XIVELY_FEED_ID] + ".json"
    xivelyHeaders = {'content-type': 'application/json', 'X-ApiKey': currentSettings[XIVELY_API_KEY]}

    # Register location and metadata
    if xivelyEnabled:
        data = {"version": "1.0.0", 
                "tags": ["Automagically"], 
                "description": "Things should just work - Automagically",
                "website": "http://automagically.weebly.com/",
                "location" : {"domain": "physical", "lat":lat, "lon":lon} }
        try:
            conn = httplib.HTTPSConnection(xivelyApi)
            conn.request('PUT', xivelyPage, body=json.dumps(data), headers=xivelyHeaders)
        except:
            print 'Request in xively plugin threw an exception'
    
    # Main loop
    while(keepRunning):
        try:
            try:
                if xivelyEnabled and nextUpdate > 0:
                    s = workQueue.get(True, nextUpdate) # Wait for a signal or a timeout
                else:
                    s = workQueue.get(True) # Wait for a signal
                
            except:
                # Exception occurs when nextUpdate time has elapsed
                s = 'TIMEOUT'
                if debug:
                    print PLUGIN_NAME + ' time to push data to xively servers'

            if s == None: # Time to close down
                if debug:
                    print 'Got a none from workQueue'
                workQueue.task_done()
                keepRunning = False
                continue
                
            elif s == 'generalupdate': # Configuration has changed, update to new values
                if debug:
                    print 'General settings updated, affecting ' + PLUGIN_NAME
                lat=str(getSetting('Location Latitude'))
                lon=str(getSetting('Location Longitude'))
                if xivelyEnabled:
                    data = {"version": "1.0.0", 
                            "tags": ["Automagically"], 
                            "description": "Things should just work - Automagically",
                            "website": "http://automagically.weebly.com/",
                            "location" : {"domain": "physical", "lat":lat, "lon":lon} }
                    try:
                        conn = httplib.HTTPSConnection(xivelyApi)
                        conn.request('PUT', xivelyPage, body=json.dumps(data), headers=xivelyHeaders)
                    except:
                        print 'Request in xively plugin threw an exception'
                workQueue.task_done()
                continue

            elif s == 'update': # Configuration has changed, update to new values
                if debug:
                    print 'Settings updated for ' + PLUGIN_NAME
                    
                currentSettings = getSettings(PLUGIN_NAME)
                xivelyEnabled = currentSettings[XIVELY_ENABLED]
                nextUpdate = currentSettings[XIVELY_INTERVAL]
                xivelyPage = "/v2/feeds/" + currentSettings[XIVELY_FEED_ID] + ".json"
                xivelyHeaders = {'content-type': 'application/json', 'X-ApiKey': currentSettings[XIVELY_API_KEY]}
                workQueue.task_done()
                continue
    
            elif s == 'TIMEOUT': # Update xively based on variables that are not "hidden"
                data = { "version": "1.0.0", "datastreams": [] }
                varList = GlobalVariable.objects.filter(hidden = False)
                if varList:
                    for var in varList:
                        if debug:
                            print "Uploading " + var.name + "="+ str(var.getValue()) + " to Xively as " + allowedCharactersTransform(var.name)
                        data["datastreams"].append({"id": allowedCharactersTransform(var.name), "unit":{"label": var.unit}, "current_value": str(var.getValue())})

                    try:
                        conn = httplib.HTTPSConnection(xivelyApi)
                        conn.request('PUT', xivelyPage, body=json.dumps(data), headers=xivelyHeaders)
                    except:
                        print 'Request in xively plugin threw an exception'
                continue
                
            else: # Update xively based on input from signal, format: NAME:VALUE:UNIT,NAME:VALUE:UNIT or NAME:VALUE, NAME:VALUE
                if debug:
                    print PLUGIN_NAME + ' got signal:' + s
                if xivelyEnabled:
                    doUpdate = False
                    data = { "version": "1.0.0", "datastreams": [] }
                    for var in s.split(','):
                        if var != '':
                            keyVal=var.split(':')
                            if len(keyVal) >= 2:
                                if debug:
                                    print "Uploading values for key " + str(keyVal[0]) + " with value " + str(keyVal[1])
                                doUpdate = True
                                if len(keyVal) == 3:
                                    data["datastreams"].append({"id": allowedCharactersTransform(str(keyVal[0])), "unit":{"label": str(keyVal[2])}, "current_value": str(keyVal[1])})
                                else:
                                    data["datastreams"].append({"id": allowedCharactersTransform(str(keyVal[0])), "current_value": str(keyVal[1])})
                    if doUpdate:
                        try:
                            conn = httplib.HTTPSConnection(xivelyApi)
                            conn.request('PUT', xivelyPage, body=json.dumps(data), headers=xivelyHeaders)
                        except:
                            print 'Request in xively plugin threw an exception'
                        
                    
                workQueue.task_done()
                continue

        except:
            print 'Error in ' + PLUGIN_NAME + ' threadfunction'
            if debug:
                raise

def init():
    settings = {XIVELY_ENABLED:     ('boolean', False),
                XIVELY_FEED_ID:     ('string', ''),
                XIVELY_API_KEY:     ('string', ''),
                XIVELY_INTERVAL:    ('integer', 60)}
    return ([PLUGIN_NAME, 'general'], settings, signalHandler, threadFunc)
